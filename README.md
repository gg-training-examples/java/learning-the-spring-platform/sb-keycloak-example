# Spring Boot KeyCloak Example

## Reference
- [Spring Boot Adapter](https://www.keycloak.org/docs/latest/securing_apps/index.html#_spring_boot_adapter)
- [Spring Security Adapter](https://www.keycloak.org/docs/latest/securing_apps/index.html#_spring_security_adapter)
