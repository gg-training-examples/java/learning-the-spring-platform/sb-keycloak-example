package com.groundgurus.sbkeycloakexample.controller;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;

import java.util.List;

@Controller
public class SimpleController {
  @GetMapping("/")
  String index() {
    return "index";
  }

  @GetMapping("/products")
  String products(Model model) {
    model.addAttribute("products", List.of("iPad", "iPhone", "iWatch"));
    return "products";
  }
}
